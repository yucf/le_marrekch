export interface Meal {
    _id: string,
    name: String,
    description: String,
    image:String,
    price:number,
    quantity:number
  
  }